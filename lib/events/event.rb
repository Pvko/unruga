class EVENT

  attr_accessor :event_id, :wildcard, :execute_count, :description, :script

  def initialize(event_id=0, wildcard=false, exec_count=1, description="placeholder", &script)
    @eid = event_id
    @wc = wildcard
    @ec = exec_count
    @des = description
    if block_given?
      @script = script
    end
  end

  def execute
    @p = Proc.new @script
    @p.call
  end

end