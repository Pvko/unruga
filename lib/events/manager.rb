require_relative 'event'

class MANAGER

  event_instance = EVENT.new()

  def !replace_event

  end

  def !remove_event

  end

  def !skip_event

  end

  def skip_event

  end

  def replace_event()

  end

  def remove_event()

  end

  def slice_queue()

  end

  def empty_event_queue

  end

  def empty_container

  end

  def halt_cycle

  end

  def cycle_container

  end

  def repeat_event

  end

  def start_event(container=0, event)
    @cont = container
    @event = event


  end


  def stop_event

  end
end